//Task 1
let r = 4;

let c = Math.PI*2*r;

console.log(c)

//Task 2
let animalString = "cameldogcatlizard";
let andString = " and ";

console.log(animalString.substring(8,11) + andString + animalString.substring(5,8))

//Task 3
let person = {
  firstName : "Kanye",
  lastName : "West",
  birthDate : "8 June 1977",
  annIncome : 150000000
}

console.log(person.firstName + " " + person.lastName + " was born on " + person.birthDate + " and has an annual income of $" + person.annIncome)

//Task 4
var number1, number2;

//RHS generates a random number between 1 and 10 inclusive
number1 = Math.floor((Math.random() * 10) + 1);
//RHS generates a random number between 1 and 10 inclusive
number2 = Math.floor((Math.random() * 10) + 1);
console.log("number1 = " + number1 + " number2 = " + number2);

let number3 = number1;
number1 = number2;
number2 = number3;

console.log("number1 = " + number1 + " number2 = " + number2);

//Task 5
let year;
let yearNot2015Or2016;
year = 2015;
yearNot2015Or2016 = (year !== 2015) && (year !== 2016);

console.log(yearNot2015Or2016);















