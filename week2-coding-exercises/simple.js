//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, 65, -14, -19, -51, -17, -25]
    //pos odd neg even
    let posOdd = []
    let negEven = []
    let posOddAns = "Positive Odd : "
    let negEvenAns = "Negative Even : "
    let j = 0
    let k = 0
    for (let i = 0; i < myArray.length; i++) {
        if(myArray[i] > 0){
            if(myArray[i] % 2 !== 0){
                posOdd[j] = myArray[i]
                j++
            }
        }
        else{
            if(myArray[i] % 2 === 0){
                negEven[k] = myArray[i]
                k++
            }
        }
    }
    
    for(let i = 0; i < posOdd.length; i++){
        posOddAns += posOdd[i] 
        if(i != (posOdd.length - 1)){
        posOddAns += ", "
        }
    }
     
    for(let i = 0; i < negEven.length; i++){
        negEvenAns += negEven[i] 
        if(i != (negEven.length - 1)){
        negEvenAns += ", "
        }
    }
    
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    output = posOddAns
    outPutArea.innerText += output + "\n"    //this line will fill the above element with your output.
    output = negEvenAns
    outPutArea.innerText += output + "\n"   
}

function question2(){
    let output = "" 
    
    //Question 2 here 
     let d1 = 0
    let d2 = 0
    let d3 = 0
    let d4 = 0
    let d5 = 0
    let d6 = 0
    let answer = 0
    
    for(let i = 0; i < 60000; i++){
        answer = Math.floor((Math.random() * 6) + 1)
        if(answer === 1){
            d1++
        }
        else if(answer === 2){
            d2++
        }
        else if(answer === 3){
            d3++
        }
         else if(answer === 4){
            d4++
        }
         else if(answer === 5){
            d5++
        }
         else if(answer === 6){
            d6++
        }
        
    }
    let outPutArea = document.getElementById("outputArea2") 
    
    output = "Frequency of die rolls "
    outPutArea.innerText += output + "\n"   
    output = "1: " + d1
    outPutArea.innerText += output + "\n"   
    output = "2: " + d2
    outPutArea.innerText += output + "\n"   
    output = "3: " + d3
    outPutArea.innerText += output + "\n"   
    output = "4: " + d4
    outPutArea.innerText += output + "\n"   
    output = "5: " + d5
    outPutArea.innerText += output + "\n"   
    output = "6: " + d6
    outPutArea.innerText += output + "\n"   
    
    
    
   
    
}

function question3(){
    let output = ""
    let answers = [0,0,0,0,0,0,0]
    for(let i = 0; i < 600000; i++){
        answers[Math.floor((Math.random() * 6) + 1)]++
    }
    
    let outPutArea = document.getElementById("outputArea3")
    
    output = "Frequency of die rolls "
    outPutArea.innerText += output + "\n"  
    for(let i = 1; i <= 6; i++){
        output = i + ": " + answers[i]
        outPutArea.innerText += output + "\n"   
    }
    
    
   
    
    
    
   
    
    //Question 3 here 
    
    
    
}

function question4(){
    let output = "" 
    
    let dieRolls = {
        Frequencies: {
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        Total:60000,
        Exceptions: ""
    }   
    
    let errorArray = []
    let j = 0
    
    for(let i = 0; i < dieRolls.Total; i++){
        let roll = ""
        roll += Math.floor((Math.random() * 6) + 1)        
        dieRolls.Frequencies[roll]++
    }
    
    for(let i = 1; i <= 6; i++){
        let roll = ""
        roll += i
        if(Math.abs(dieRolls.Frequencies[roll] - (dieRolls.Total / 6)) > (0.01*(dieRolls.Total/6))){
            errorArray[j] = roll
            j++
        }
        
    }
    
    for(let i = 0; i < errorArray.length; i++){
    dieRolls.Exceptions += errorArray[i] + " "
    }
           
    let outPutArea = document.getElementById("outputArea4") 
    output = "Frequency of dice rolls\n"
    outPutArea.innerText = output 
    for (prop in dieRolls){
        output = prop + " = " + dieRolls [prop] + "\n"      
        if(prop === "Frequencies"){
            for(let i = 1; i <= 6 ; i++){
                n = ""
                n+=i
                output = dieRolls.Frequencies[n] + "\n"
                outPutArea.innerText += output  
            }
           
        }
        outPutArea.innerText += output         
    }  
}

function question5(){
    let output = "" 
    let person = {
        name: "Jane",
        income: 127050
    }
    
    let tax = 0
    if(person.income < 18200){
        tax = 0
    }
    else if(person.income < 37000){
        tax = 0.19 * (person.income - 18200)
    }
     else if(person.income < 90000){
        tax = 3572 + 0.325 * (person.income - 37000)
    }
    
     else if(person.income < 180000){
        tax = 20797 + 0.37 * (person.income - 90000)
    }
    
     else if(person.income < 37000){
        tax = 54097 + 0.45 * (person.income - 18200)
    }
    
    
    
    
    let outPutArea = document.getElementById("outputArea5") 
    output = person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + tax
    outPutArea.innerText = output 
}